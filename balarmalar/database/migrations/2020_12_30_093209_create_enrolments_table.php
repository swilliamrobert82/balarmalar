<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnrolmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolments', function (Blueprint $table) {
            $table->id();
            $table->integer('year');
            $table->string('school_location');
            $table->string('given_name');
            $table->string('middle_name');
            $table->string('family_name');
            $table->string('gender', 10);
            $table->string('given_name_tamil');
            $table->string('middle_name_tamil');
            $table->string('family_name_tamil');
            $table->date('date_of_birth');
            $table->string('proficiency_in_tamil');
            $table->boolean('formal_education_in_tamil');
            $table->boolean('consent_in_email');
            $table->text('home_address');
            $table->string('mainstream_school_name');
            $table->string('mainstream_school_class');
            $table->text('mainstream_school_address');
            $table->string('mainstream_school_suburb');
            $table->boolean('medical_asthma');
            $table->string('asthma_medication');
            $table->string('major_illness_disability');
            $table->string('allergies');
            $table->string('medications');
            $table->string('allergies_to_medication');
            $table->boolean('self_declaration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrolments');
    }
}
