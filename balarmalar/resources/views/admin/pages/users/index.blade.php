@extends('admin.layouts.app')

@section('content')

    <section class="content-header"  style="padding: 10px;">
        <h1>
              <a class="btn btn-success" href="{{ route('users.create') }}">
                <i class="fa fa-plus"></i> Add
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Team Management</a></li>
            <li class="active">Team</li>
        </ol>
    </section>


<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List Team</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody><tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>
                            @foreach($user->roles()->get()->pluck('name')->toArray() as $role)
                                <span class="badge badge-primary" style="background-color: #283a97 !important; font-size: 14px;">{{ $role }}</span>
                            @endforeach
                        </td>
                        <td>
                            <a href="{{ route('users.edit',[$user->id]) }}"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                            <a href="{{ route('users.delete',[$user->id]) }}"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                          </td>
                    </tr>
                    @endforeach

                    </tbody></table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection
