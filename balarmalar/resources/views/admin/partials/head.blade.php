<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ config('app.name') }} | {{ $nav }}</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('panel-resources/dist/css/BoolFalse.min.css') }}">
<link rel="stylesheet" href="{{ asset('panel-resources/dist/css/tags.css') }}">
<!-- BoolFalse Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ asset('panel-resources/dist/css/skins/_all-skins.min.css') }}">
<!-- Morris chart -->
{{--<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/morris.js/morris.css') }}">--}}
<!-- jvectormap -->
{{--<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/jvectormap/jquery-jvectormap.css') }}">--}}
<!-- Date Picker -->
{{--<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">--}}
<!-- Daterange picker -->
{{--<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">--}}
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('panel-resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('panel-resources/bower_components/select2/dist/css/select2.min.css') }}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="{{ asset('panel-resources/dist/js/bundle.min.js') }}"></script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
