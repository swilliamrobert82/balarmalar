<aside class="main-sidebar">
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Admin Dashboard</li>

            <li class="{{ $nav == 'dashboard' ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i
                        class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li class="@if(request()->is('admin/users') || request()->is('admin/users/*')) active @endif"><a
                    href="{{ route('users') }}"> <i
                        class="fa fa-user"></i> <span>Users Management</span></a></li>


        </ul>
    </section>
</aside>
