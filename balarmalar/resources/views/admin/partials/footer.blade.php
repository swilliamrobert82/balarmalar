<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>{{ config('app.name') }}</b>
    </div>
    <strong>Copyright &copy; {{ '2019' . (date('Y') == 2019 ? '' : '-' . date('Y')) }}</strong> All rights reserved.
</footer>
<!-- jQuery -->
<script src="{{ asset('panel-resources/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('panel-resources/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('panel-resources/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<!-- Sparkline -->
<!-- JQVMap -->
<!-- jQuery Knob Chart -->
<!-- daterangepicker -->
<script src="{{ asset('panel-resources/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('panel-resources/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->

<!-- Summernote -->
<!-- overlayScrollbars -->
<script src="{{ asset('panel-resources/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('panel-resources/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ asset('panel-resources/dist/js/pages/dashboard.js') }}"></script>--}}
<!-- AdminLTE for demo purposes -->

