<?php
namespace App\Repositories\Enrolment;

use App\Models\Enrolment;
use App\Repositories\School\EnrolmentRepositoryInterface;

class EnrolmentRepository implements EnrolmentRepositoryInterface
{
    protected $model;

    public function __construct(Enrolment $model)
    {
        $this->model = $model;
    }

}
