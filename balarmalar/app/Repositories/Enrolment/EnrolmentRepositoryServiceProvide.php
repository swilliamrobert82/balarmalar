<?php


namespace App\Repositories\Enrolment;


use Illuminate\Support\ServiceProvider;

class EnrolmentRepositoryServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Enrolment\EnrolmentRepositoryInterface', 'App\Repositories\Enrolment\EnrolmentRepository');
    }
}
