<?php
namespace App\Repositories\ParentGuardian;

use App\Models\ParentGuardian;

class ParentGuardianRepository implements ParentGuardianRepositoryInterface
{
    protected $model;

    public function __construct(ParentGuardian $model)
    {
        $this->model = $model;
    }

}
