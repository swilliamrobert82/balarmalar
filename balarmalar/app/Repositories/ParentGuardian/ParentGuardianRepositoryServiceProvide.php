<?php


namespace App\Repositories\ParentGuardian;


use Illuminate\Support\ServiceProvider;

class ParentGuardianRepositoryServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\ParentGuardian\ParentGuardianRepositoryInterface', 'App\Repositories\ParentGuardian\ParentGuardianRepository');
    }
}
