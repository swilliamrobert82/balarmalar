<?php


namespace App\Repositories\Membership;


use Illuminate\Support\ServiceProvider;

class MembershipRepositoryServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Membership\MembershipRepositoryInterface', 'App\Repositories\Membership\MembershipRepository');
    }
}
