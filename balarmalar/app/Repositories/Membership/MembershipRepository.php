<?php
namespace App\Repositories\Membership;

use App\Models\Membership;

class MembershipRepository implements SchoolRepositoryInterface
{
    protected $model;

    public function __construct(Membership $model)
    {
        $this->model = $model;
    }

}
