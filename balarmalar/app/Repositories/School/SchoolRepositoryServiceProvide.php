<?php


namespace App\Repositories\School;


use Illuminate\Support\ServiceProvider;

class SchoolRepositoryServiceProvide extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\School\SchoolRepositoryInterface', 'App\Repositories\School\SchoolRepository');
    }
}
