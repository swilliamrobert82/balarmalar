<?php
namespace App\Repositories\School;

use App\Models\School;

class SchoolRepository implements SchoolRepositoryInterface
{
    protected $model;

    public function __construct(School $model)
    {
        $this->model = $model;
    }

}
