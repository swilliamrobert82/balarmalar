<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ParentGuardianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ParentGuardian  $parentGuardian
     * @return \Illuminate\Http\Response
     */
    public function show(ParentGuardian $parentGuardian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ParentGuardian  $parentGuardian
     * @return \Illuminate\Http\Response
     */
    public function edit(ParentGuardian $parentGuardian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ParentGuardian  $parentGuardian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParentGuardian $parentGuardian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ParentGuardian  $parentGuardian
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParentGuardian $parentGuardian)
    {
        //
    }
}
