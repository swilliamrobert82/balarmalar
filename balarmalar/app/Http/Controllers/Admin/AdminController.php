<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        View::share('nav', ''); //ss
    }


    public function index()
    {
        $users = User::all();
        return view('admin.pages.users.index', compact('users')); //ss
    }

    public function create(){
        $roles = Role::all();
        return view('admin.pages.users.create', compact('roles'));
    }

    public function store(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:members'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make('password')
        ]);

      //  print_r($request->roles);
       // \DB::enableQueryLog(); // Enable query log
        $role = Role::select('id')->where('name', $request->role)->first();
        $user->roles()->attach($role);
        return redirect()->route('users');

    }

    public function edit($id)
    {

        $roles = Role::all();
        $user = User::find($id);
        return view('admin.pages.users.edit', compact('roles', 'user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->roles()->sync($request->roles);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect(route('users'))->with('success', ucwords($request->name) . ' - details successfully updated!');

    }

    public function destroy($id)
    {

        $user = User::find($id);
        $user->roles()->detach();
        User::destroy($id);
        return redirect()->route('users');
    }


}
