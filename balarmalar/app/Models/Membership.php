<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $table = 'memberships';

    public function school()
    {
        $this->belongsTo('App\Models\School', 'school_code', 'school_code');
    }
}
