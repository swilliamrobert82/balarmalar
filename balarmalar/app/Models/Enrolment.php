<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enrolment extends Model
{
    protected $table = 'enrolments';

    public function school()
    {
        $this->belongsTo('App\Models\School', 'school_code', 'school_location');
    }
}
