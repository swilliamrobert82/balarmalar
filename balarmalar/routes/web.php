<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/admin/dashboard', 'App\Http\Controllers\Admin\DashboardController@index')->name('dashboard');

Route::get('/admin/users', 'App\Http\Controllers\Admin\AdminController@index')->name('users');
Route::get('/admin/users/create', 'App\Http\Controllers\Admin\AdminController@create')->name('users.create');
Route::post('/admin/users/store', 'App\Http\Controllers\Admin\AdminController@store')->name('users.store');
Route::get('/admin/users/edit/{id}', 'App\Http\Controllers\Admin\AdminController@edit')->name('users.edit');
Route::post('/admin/users/update/{id}', 'App\Http\Controllers\Admin\AdminController@update')->name('users.update');
Route::get('/admin/users/delete/{id}', 'App\Http\Controllers\Admin\AdminController@destroy')->name('users.delete');
